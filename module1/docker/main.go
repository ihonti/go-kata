package main

import "fmt"

func FinSmal(n int) int {
Outer:

	for n++; ; n++ {
		for i := 2; ; i++ {
			switch {
			case i*i > n:
				break Outer
			case n%i == 0:
				continue Outer
			}
		}
	}
	return n
}
func main() {
	for i := 90; i < 92; i++ {
		n := FinSmal(i)
		fmt.Print("Prime num: ")
		fmt.Println(i, "is", n)
	}
}
