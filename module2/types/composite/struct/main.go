package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	Age      int
	Name     string
	Wallet   Wallet
	Lacation Lacation
}

type Lacation struct {
	Address string
	City    string
	Index   string
}

type Wallet struct {
	RUR uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func main() {
	wallet := Wallet{
		RUR: 250000,
		USD: 3500,
		BTC: 1,
		ETH: 4,
	}
	user := User{
		Age:    13,
		Name:   "Alexander",
		Wallet: wallet,
	}
	user2 := User{
		Age:  34,
		Name: "Anton",
		Wallet: Wallet{
			RUR: 144000,
			USD: 38900,
			BTC: 55,
			ETH: 34,
		},
		Lacation: Lacation{
			Address: "Невесёловская 3-я ул, 13, к.2",
			City:    "Москва",
			Index:   "108836",
		},
	}
	fmt.Println(wallet)
	fmt.Println("wallet allocates:", unsafe.Sizeof(wallet), "bytes")
	fmt.Println(user)
	fmt.Println(user2)
}
