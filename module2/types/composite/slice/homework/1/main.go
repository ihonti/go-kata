package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	//second solution Append func
	//Append(&s)
	s = Append(s)
	fmt.Println(s)
}

// Append func second solution
//
//	func Append(s *[]int) {
//		*s = append(*s, 4)
//	}
func Append(s []int) []int {
	s = append(s, 4)
	return s
}
