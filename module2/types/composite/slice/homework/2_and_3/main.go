package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	// first task
	for i, user := range users {
		if user.Age > 40 && i < len(users)-1 {
			users = append(users[:i], users[i+1:]...)
		} else if user.Age > 40 && i == len(users)-1 {
			users = users[:len(users)-1]
		}
	}
	fmt.Println(users)
	//second task
	users = users[1:] // del first user
	fmt.Println(users)
	users = users[:len(users)-1] // del last user
	fmt.Println(users)
}
