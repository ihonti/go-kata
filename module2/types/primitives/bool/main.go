package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeBool()
}

func typeBool() {
	fmt.Println("\n=== START type bool ===")
	var b bool
	fmt.Println("Значение типа bool по умолчанию =", b, "\nРазмер в  байтах:", unsafe.Sizeof(b))
	var u uint8 = 1
	fmt.Println(b)
	b = *(*bool)(unsafe.Pointer(&u))
	fmt.Println(b)
	fmt.Println("=== END type bool ===")
}
