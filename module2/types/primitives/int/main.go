package main

import (
	"fmt"
	"unsafe"
)

func main() {
	var n int8
	fmt.Println("size is", unsafe.Sizeof(n), "bytes")
	typeInt()
}

func typeInt() {
	fmt.Println("=== START type int ===")
	var uintNum8 uint8 = 1 << 7
	var min8 = int8(uintNum8)
	uintNum8--
	var max8 = int8(uintNum8)
	fmt.Println(min8, max8)
	fmt.Println("int8 min value", min8, "int8 max value", max8, "size:", unsafe.Sizeof(min8), "byte")
	var uintNum16 uint16 = 1 << 15
	var min16 = int16(uintNum16)
	uintNum16--
	var max16 = int16(uintNum16)
	fmt.Println(min16, max16)
	fmt.Println("int16 min value", min16, "int16 max value", max16, "size:", unsafe.Sizeof(min16), "byte")
	var uintNum32 uint32 = 1 << 31
	var min32 = int32(uintNum32)
	uintNum32--
	var max32 = int32(uintNum32)
	fmt.Println(min32, max32)
	fmt.Println("int32 min value", min32, "int32 max value", max32, "size:", unsafe.Sizeof(min32), "byte")
	var uintNum64 uint64 = 1 << 63
	var min64 = int64(uintNum64)
	uintNum64--
	var max64 = int64(uintNum64)
	fmt.Println(min64, max64)
	fmt.Println("int64 min value", min64, "int64 max value", max64, "size:", unsafe.Sizeof(min64), "byte")
	fmt.Println("=== END type int ===")

}
