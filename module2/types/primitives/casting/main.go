package main

import "fmt"

func main() {
	typeConvert()
}

func typeConvert() {
	var numFloat float32
	var numInt int32 = 3
	var numInt2 int
	numFloat = float32(numInt)
	fmt.Printf("тип: %T, значение: %v\n", numFloat, numFloat)
	numInt2 = int(numFloat)
	fmt.Println(numInt2)
	fmt.Printf("float32 convert to %T\nint32 convert to %T\nint convert to %T\n",
		uint8(numFloat), float64(numInt), uint16(numInt2))
}
