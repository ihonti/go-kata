package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println(n, "size is", unsafe.Sizeof(n), "bytes")
	typeUint()
	typeByte()
}

func typeUint() {
	fmt.Println("=== START type uint ===")
	var numberUint8 uint8 = 1 << 1
	var number2Uint8 uint8 = 1 >> 1
	fmt.Println("left shift uint8:", numberUint8, "size:", unsafe.Sizeof(numberUint8), "bytes",
		"\nright shift uint8:", number2Uint8, "size:", unsafe.Sizeof(number2Uint8), "bytes")
	numberUint8 = (1 << 8) - 1
	fmt.Println("uint8 max value:", numberUint8, "size:", unsafe.Sizeof(numberUint8), "bytes")
	var numberUint16 uint16 = (1 << 16) - 1
	var numberUint32 uint32 = (1 << 32) - 1
	var numberUint64 uint64 = (1 << 64) - 1
	fmt.Println("uint16 max value:", numberUint16, "size:", unsafe.Sizeof(numberUint16), "bytes")
	fmt.Println("uint32 max value:", numberUint32, "size:", unsafe.Sizeof(numberUint32), "bytes")
	fmt.Println("uint64 max value:", numberUint64, "size:", unsafe.Sizeof(numberUint64), "bytes")
	fmt.Println("=== END type uint ===")
}

func typeByte() {
	fmt.Println("\n=== START type byte ===")
	var b byte = 124
	fmt.Println("Размер в  байтах:", unsafe.Sizeof(b))
	fmt.Println("=== END type byte ===")
}
