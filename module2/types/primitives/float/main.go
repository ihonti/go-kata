package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeFloat()
}

func typeFloat() {
	fmt.Println("=== START type float ===")
	var floatNumber float32
	var uintNumber uint32 = 1 << 29 // сдвиг на 29 позиций в 30 ячейку
	uintNumber += 1 << 28           // единицу в 29 ячейку
	uintNumber += 1 << 27           // единицу в 28 ячейку
	uintNumber += 1 << 26
	uintNumber += 1 << 25
	// мантиса
	uintNumber += 1 << 21 // в 22 позицию
	floatNumber = *(*float32)(unsafe.Pointer(&uintNumber))
	fmt.Print(floatNumber, " ")
	uintNumber += 1 << 31 // старший (32) бит в 1 для знака минус
	floatNumber = *(*float32)(unsafe.Pointer(&uintNumber))
	fmt.Print(floatNumber, " ")
	uintNumber -= 1 << 31 // старший (32) бит в 1 для знака в плюс
	floatNumber = *(*float32)(unsafe.Pointer(&uintNumber))
	fmt.Println(floatNumber)

	// примеры ошибок, и почему не стоит пользоваться чпз
	a, b := 2.3329, 2.1234
	c := a + b
	fmt.Println("Пример ошибки 1", c)

	a = 9.999999
	b2 := float64(a)
	fmt.Println("Пример ошибки 2", b2)

	a = 999998455
	b3 := float32(a)
	fmt.Printf("Пример ошибки 3: %f\n", b3)

	x := 5.2
	y := 4.1
	fmt.Print(x+y, " ---it works--- ")
	fmt.Println((x + y) == 9.3)

	d := 5.2
	z := 2.1
	fmt.Print(d+z, " ---error--- ")
	fmt.Println((d + z) == 7.3)
	fmt.Println("=== END type float ===")
}
